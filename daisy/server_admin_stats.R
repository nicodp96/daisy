#
# STATISTICS TAB
#


stats <- function(input, output){

  statstb$downloaddate =  as.Date(statstb$downloaddate, format = "%Y-%m-%d")

  output$uicpn_admin_stats <- renderUI(
		div(class="page-container",
      fluidRow(
      column(6,
  			h2(class="title_bold", "Download Statistics")
      ),
      column(6,
        br(),br(),
        wellPanel(
          dateRangeInput('dateRange',
                         label = 'Filter by date',
                         start = as.Date('2018-01-01') , end = as.Date(Sys.Date())
          )
        )
      )),

      if(empty(statstb)){

        tabsetPanel(type="tab",
          tabPanel("Overview",
            br(),
            strong(paste("No data to display."))
          ),
          tabPanel("By OS",
            br(),
            strong(paste("No data to display."))
          ),
          tabPanel("By Version",
            br(),
            strong(paste("No data to display."))
          ),
          tabPanel("By Application field",
            br(),
            strong(paste("No data to display."))
          ),
          tabPanel("By Country",
            br(),
            strong(paste("No data to display."))
          ),
          tabPanel("Recurring downloads",
            br(),
            strong(paste("No data to display."))
          )
        )

      }else{

        tabsetPanel(type="tab",
          tabPanel("Overview",
            plotOutput("plot_overview"),
            selectInput('groupby', "",
                        c("By Month" = "month",
                          "By Year" = "year"
                        ), selected = "month"
            )
          ),

          tabPanel("By OS",
            checkboxInput("versions", "Highlight Download versions", value = FALSE),
            plotOutput("plot_byOS")
          ),
          tabPanel("By Version",
            checkboxInput("os", "Highlight Operating Systems", value = FALSE),
            plotOutput("plot_byVersion")
          ),
          tabPanel("By Application field",
            plotOutput("plot_byApplicationField")
          ),
          tabPanel("By Country",
            plotOutput("plot_byCountry")
          ),
          tabPanel("Recurring downloads",
            plotOutput("plot_recurringDownloads")
          )
        )
      }

      #TODO: delete when no longer necessary
      #div(
      #  actionButton("btn_downloaddatabase", "Check Database", class="btn btn-warning btn-block"),
      #  style="text-align: center;"
      #)
  ))

  #TODO: delete when no longer necessary
  ## DATABASE BUTTON
  	#observeEvent(input$btn_downloaddatabase, {
  	#	showModal(modalDialog(
  	#		renderDataTable(renderStats)
  	#	))
  	#})

    output$plot_overview <- renderPlot({
      renderStats <<- statstb %>% filter(downloaddate >= input$dateRange[1] & downloaddate <= input$dateRange[2])

      #adds month column to the database
      renderStats <<- renderStats %>% mutate(month = as.character(month(downloaddate)))

      #adds year column to the database
      renderStats <<- renderStats %>% mutate(year = as.character(year(downloaddate)))

      if(input$groupby == "month"){

          ggplot(data = renderStats) + geom_bar(mapping = aes(x = month, fill=year)) +
            facet_wrap(~ year, ncol = 2) +
            scale_y_continuous(breaks = function(x) unique(floor(pretty(seq(0, (max(x) + 1) * 1.1))))) +
            scale_x_discrete(labels=c("1"="Jan", "2"="Feb", "3"="Mar", "4"="Apr", "5"="May", "6"="Jun", "7"="Jul", "8"="Aug", "9"="Sep", "10"="Oct", "11"="Nov", "12"="Dec")) +
            labs(x = "Month", y = "") +
            scale_fill_brewer(palette="Set2") +
            theme_minimal()
        } else {
        if(input$groupby == "year"){

           ggplot(data = renderStats) + geom_bar(mapping = aes(x = year, fill = year)) +
            scale_y_continuous(breaks = function(x) unique(floor(pretty(seq(0, (max(x) + 1) * 1.1))))) +
            labs(x = "Year", y = "") +
            scale_fill_brewer(palette="Set2") +
            theme_minimal()

        }
        }
    })

    output$plot_byOS <- renderPlot({

       renderStats <<- statstb %>% filter(downloaddate >= input$dateRange[1] & downloaddate <= input$dateRange[2])

       if(input$versions ==TRUE){

        ggplot(data = renderStats) + geom_bar(mapping = aes(x = operatingsystem, fill = downloadversion)) +
          scale_y_continuous(breaks = function(x) unique(floor(pretty(seq(0, (max(x) + 1) * 1.1))))) +
          labs(x = "Operating System", y = "", fill = "Download\nVersion") +
          scale_fill_brewer(palette="Set2") +
          theme_minimal()
       }
       else{

        ggplot(data = renderStats) + geom_bar(mapping = aes(x = operatingsystem, fill = operatingsystem)) +
          scale_y_continuous(breaks = function(x) unique(floor(pretty(seq(0, (max(x) + 1) * 1.1))))) +
          labs(x = "Operating System", y = "") +
          scale_fill_brewer(palette="Set2") +
          theme_minimal() + guides(fill=FALSE)
       }
    })

    output$plot_byVersion <- renderPlot({

        renderStats <<- statstb %>% filter(downloaddate >= input$dateRange[1] & downloaddate <= input$dateRange[2])

        if(input$os == TRUE){

         ggplot(data = renderStats) + geom_bar(mapping = aes(x = downloadversion, fill = operatingsystem)) +
            scale_y_continuous(breaks = function(x) unique(floor(pretty(seq(0, (max(x) + 1) * 1.1))))) +
            labs(x = "Download Version", y = "", fill = "Operating\nSystem") +
            scale_x_discrete(labels = c("1.8" = "version 1.8","1.9" = "version 1.9")) +
            scale_fill_brewer(palette="Set2") +
            theme_minimal()
        }
        else{
         ggplot(data = renderStats) + geom_bar(mapping = aes(x = downloadversion, fill = downloadversion)) +
         scale_y_continuous(breaks = function(x) unique(floor(pretty(seq(0, (max(x) + 1) * 1.1))))) +
         labs(x = "Download Version", y = "") +
         scale_x_discrete(labels = c("1.8" = "version 1.8","1.9" = "version 1.9")) +
         scale_fill_brewer(palette="Set2") +
         theme_minimal() + guides(fill=FALSE)


       }
    })

    output$plot_byApplicationField <- renderPlot({

      renderStats <<- statstb %>% filter(downloaddate >= input$dateRange[1] & downloaddate <= input$dateRange[2])

       tmp <- data.frame(lapply(renderStats, as.character), stringsAsFactors=FALSE)
       renderStats <<- group_by(tmp, applfield, add = FALSE)

       ggplot(data = renderStats) + geom_bar(mapping = aes(x = applfield, fill = applfield)) +
          scale_y_continuous(breaks = function(x) unique(floor(pretty(seq(0, (max(x) + 1) * 1.1))))) +
          labs(x = "Application Field", y = "") +
          scale_x_discrete(labels = c("matres" = "Mathematical Research", "biores" = "Biological Research", "other" = "Other")) +
          scale_fill_brewer(palette="Set2") +
          theme_minimal() + guides(fill=FALSE)
    })

    output$plot_byCountry <- renderPlot({

      renderStats <<- statstb %>% filter(downloaddate >= input$dateRange[1] & downloaddate <= input$dateRange[2])

       tmp <- data.frame(lapply(renderStats, as.character), stringsAsFactors=FALSE)
       renderStats <<- group_by(tmp, country, add = FALSE)

       ggplot(data = renderStats) + geom_bar(mapping = aes(x = country, fill = country)) +
          scale_y_continuous(breaks = function(x) unique(floor(pretty(seq(0, (max(x) + 1) * 1.1))))) +
          labs(x = "Country", y = "") +
          scale_x_discrete(labels = codeToCountry) +
          scale_fill_brewer(palette="Set2") +
          theme_minimal() + guides(fill=FALSE)
    })

    output$plot_recurringDownloads <- renderPlot({

      renderStats <<- statstb %>% filter(downloaddate >= input$dateRange[1] & downloaddate <= input$dateRange[2])

       ggplot(data = renderStats) + geom_bar(mapping = aes(x = "", fill = recurringdnld)) +
          scale_y_continuous(breaks = function(x) unique(floor(pretty(seq(0, (max(x) + 1) * 1.1))))) +
          coord_polar("y", start=0) +
          labs(x = "", y = "", fill = "Recurring\nDownloads") +
          scale_fill_brewer(palette="Set2") +
          theme_minimal()
    })

}

#
# Documentation UI
#

source("server_documentation.R")


tab_documentation = function() div(class="tab_documentation",
	class = "page-container",
	navlistPanel(
		# HTML('<hr style="border-top: 1px solid grey; margin:0px;">'),
		"Documentation",
		tabPanel("References",
			uiOutput("uicpnt_doc_references")
		),
		tabPanel("Examples",
			uiOutput("uicpnt_doc_examples")
		)
	)
)
